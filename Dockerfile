FROM node:16

WORKDIR /app
COPY . .
RUN rm -rf /app/node_modules

ENV NEON_BUILD_RELEASE=true
RUN npm install
RUN npx tsc
RUN node run.js config --bma --ipv4 localhost --port 10499

CMD ["node", "run.js", "currency-monit"]
EXPOSE 10500
EXPOSE 10499