import {MonitConstants} from "../lib/constants2";
import {DBIdentity} from "duniter/app/lib/dal/sqliteDAL/IdentityDAL";
import {showExecutionTimes} from "../lib/MonitorExecutionTime";
import {DataFinder} from "../lib/DataFinder";
import {Server} from "duniter/server";
import {PendingCert, WillMemberIdentity, WillMemberIdentityWithPendingCerts} from "./willMembers/interfaces";
import {triParDateDeDisponibilite} from "./willMembers/triParDateDeDisponibilite";
import {WotbIdCache} from "./willMembers/wotbIdCache";
import {getIdentityListOrdered} from "./willMembers/getIdentityListOrdered";
import {computeMeansAndCounts} from "./willMembers/computeMeansAndCounts";
import {getMembersQualityExt} from "./willMembers/getMembersQualityExt";
import {getSorting} from "./willMembers/getSorting";
import {SentryChecker} from "./willMembers/issuerIsSentry";
import {ConfDTO} from "duniter/app/lib/dto/ConfDTO";

// Préserver les résultats en cache
let willMembersLastUptime = 0
let identitiesList: WillMemberIdentity[] = []
let idtysPendingCertifsList: PendingCert[][] = []
let nbMaxCertifs = 0
let countMembersWithSigQtyValidCert = 0
let idtysListOrdered: WillMemberIdentityWithPendingCerts[] = []
let membersQualityExt: { [p: string]: string } = {}
let meanSentriesReachedByIdtyPerCert: number[] = []
let meanMembersReachedByIdtyPerCert: number[] = []
let cacheBeingUpdatedSince = 0
let currentBlockchainTimestamp = 0
let currentMembersCount = 0
let currentBlockNumber = -1
let limitTimestamp = 0

export async function willMembers(duniterServer: Server, days = 65, order = 'desc', sort_by = 'registrationPackage', showIdtyWithZeroCert = 'no', sortSig = 'Availability') {

  // Initaliser les constantes
  const conf: ConfDTO = duniterServer.conf;
  const dSen: number = Math.ceil(Math.pow(currentMembersCount, 1 / conf.stepMax));
  // Vérifier si le cache doit être Réinitialiser
  let reinitCache = (Math.floor(Date.now() / 1000) > (willMembersLastUptime + MonitConstants.MIN_WILLMEMBERS_UPDATE_FREQ));

  // Si le cache willMembers est dévérouillé, le vérouiller, sinon ne pas réinitialiser le cache
  if (reinitCache && !cacheBeingUpdatedSince) {
    cacheBeingUpdatedSince = new Date().getTime();
  } else if (cacheBeingUpdatedSince) {
    reinitCache = false;
  }

  if (reinitCache) {
    // En asynchrone
    rechargeCache(duniterServer, days, dSen, conf, sortSig, sort_by, order)
        .then(() => {
          console.log('Cache mis à jour')
          cacheBeingUpdatedSince = 0;
        })
        .catch((e) => {
          console.error('Exception lors de la mise à jour du cache : ' + e.message)
          console.error(e)
        })
  }

  return {
    days,
    order,
    sort_by,
    showIdtyWithZeroCert,
    sortSig,
    idtysListOrdered,
    currentBlockNumber,
    currentBlockchainTimestamp,
    currentMembersCount,
    limitTimestamp,
    dSen,
    conf,
    nbMaxCertifs,
    countMembersWithSigQtyValidCert,
    meanSentriesReachedByIdtyPerCert,
    meanMembersReachedByIdtyPerCert,
    membersQualityExt,
    cacheBeingUpdatedSince
  }
}

async function rechargeCache(duniterServer: Server, days: number, dSen: number, conf: ConfDTO, sortSig: string, sort_by: string, order: string) {

  const dataFinder = await DataFinder.getInstanceReindexedIfNecessary()
  const wotbIdCache = new WotbIdCache(dataFinder)
  // get blockchain timestamp
  let resultQueryCurrentBlock: any = await dataFinder.getCurrentBlockOrNull();
  currentBlockchainTimestamp = resultQueryCurrentBlock.medianTime;
  currentMembersCount = resultQueryCurrentBlock.membersCount;
  currentBlockNumber = resultQueryCurrentBlock.number;
  // Calculer le timestamp limite à prendre en compte
  limitTimestamp = currentBlockchainTimestamp + (days*86400);
  // Alimenter wotb avec la toile de confiance
  const wotbInstance = duniterServer.dal.wotb;

  // Réinitialiser le cache
  dataFinder.invalidateCache()
  identitiesList = [];
  idtysPendingCertifsList = [];
  nbMaxCertifs = 0;
  countMembersWithSigQtyValidCert = 0;
  willMembersLastUptime = Math.floor(Date.now() / 1000);

  // Récupérer la liste des membres référents
  const sentryChecker = new SentryChecker(wotbInstance.getSentries(dSen))

  // Récupérer la liste des identités en piscine
  const pendingMembers: DBIdentity[] = await dataFinder.findPendingMembers()

  // Récupérer pour chaque identité, l'ensemble des certifications qu'elle à reçue.
  for (const pendingIdty of pendingMembers) {
    // Extraire le numéro de bloc d'émission de l'identité
    let idtyBlockStamp = pendingIdty.buid.split("-");
    let idtyBlockNumber = idtyBlockStamp[0];

    // récupérer le medianTime et le hash du bloc d'émission de l'identité
    let idtyEmittedBlock = await dataFinder.getBlock(parseInt(idtyBlockNumber));

    // Récupérer l'identifiant wotex de l'identité (en cas d'identité multiple)
    let idties = await dataFinder.getWotexInfos(pendingIdty.uid);
    let wotexId = '';
    if (idties.length > 1) {
      let pos = 0;
      for (const idty of idties) {
        if (idty.hash == pendingIdty.hash) { wotexId = '['+pos+']'; }
        pos++;
      }
    }

    // vérifier la validité du blockstamp de l'identité
    let validIdtyBlockStamp = false;
    if (typeof(idtyEmittedBlock) == 'undefined' || idtyEmittedBlock.hash == idtyBlockStamp[1]) {
      validIdtyBlockStamp = true;
    }

    // vérifier si l'identité a été révoquée ou non
    let idtyRevoked = false;
    if (pendingIdty.revocation_sig != null) {
      idtyRevoked = true;
    }

    // Stocker les informations de l'identité
    const identity = {
      BlockNumber: parseInt(idtyBlockNumber),
      creationTimestamp: (typeof(idtyEmittedBlock) == 'undefined' ) ? currentBlockchainTimestamp:idtyEmittedBlock.medianTime,
      pubkey: pendingIdty.pubkey,
      uid: pendingIdty.uid,
      hash: pendingIdty.hash,
      wotexId: wotexId,
      expires_on: pendingIdty.expires_on || 0,
      nbCert: 0,
      nbValidPendingCert: 0,
      registrationAvailability: 0,
      validBlockStamp: validIdtyBlockStamp,
      idtyRevoked: idtyRevoked
    }
    identitiesList.push(identity);
    const pendingCertifications: PendingCert[] = []

    // récupérer l'ensemble des certifications en attente destinées à l'identité courante
    let pendingCerts = await dataFinder.findPendingCertsToTarget(pendingIdty.pubkey, pendingIdty.hash);

    // Récupérer les uid des émetteurs des certifications reçus par l'utilisateur
    // Et stocker les uid et dates d'expiration dans un tableau
    for (const pendingCert of pendingCerts) {
      // Récupérer le medianTime et le hash du bloc d'émission de la certification
      let emittedBlock = await dataFinder.getBlock(pendingCert.block_number)

      // Vérifier que l'émetteur de la certification correspond à une identité inscrite en blockchain
      let member = await dataFinder.getUidOfPub(pendingCert.from)
      if (emittedBlock && member) {
        // Récupérer la pubkey de l'émetteur
        let issuerPubkey = pendingCert.from;

        // Récupérer le wotb_id
        let wotb_id = await wotbIdCache.getWotbId(issuerPubkey)

        // Vérifier si l'émetteur de la certification est référent
        let issuerIsSentry = sentryChecker.isIssuerSentry(issuerPubkey, wotb_id);

        // Vérifier si le blockstamp est correct
        const validBlockStamp = emittedBlock.hash == pendingCert.block_hash;
        // récupérer le timestamp d'enchainement de la dernière certification écrite par l'émetteur
        let tmpQueryLastIssuerCert = await dataFinder.getChainableOnByIssuerPubkey(issuerPubkey)
        let certTimestampWritable = 0;
        if ( typeof(tmpQueryLastIssuerCert[0]) != 'undefined' && typeof(tmpQueryLastIssuerCert[0].chainable_on) != 'undefined' ) {
          certTimestampWritable = tmpQueryLastIssuerCert[0].chainable_on;
        }
        //identity.registrationAvailability = (certTimestampWritable > identity.registrationAvailability) ? certTimestampWritable : identity.registrationAvailability;

        // Vérifier que l'identité courant n'a pas déjà reçu d'autre(s) certification(s) de la part du même membre ET dans le même état de validité du blockstamp
        let doubloonPendingCertif = pendingCertifications.filter(c => c.from == member.uid && c.validBlockStamp == validBlockStamp).length > 0;
        if (!doubloonPendingCertif) {
          // Stoker la liste des certifications en piscine qui n'ont pas encore expirées
          if (pendingCert.expires_on > currentBlockchainTimestamp) {
            pendingCertifications.push({
              from: member.uid,
              pubkey: issuerPubkey,
              wotb_id: wotb_id,
              issuerIsSentry: issuerIsSentry,
              blockNumber: pendingCert.block_number,
              creationTimestamp: emittedBlock.medianTime,
              timestampExpire: pendingCert.expires_on,
              timestampWritable: certTimestampWritable,
              validBlockStamp: validBlockStamp
            });
            identity.nbCert++;
            if (validBlockStamp) { identity.nbValidPendingCert++; }
          }
        }
      }
    }
    idtysPendingCertifsList.push(pendingCertifications)

    // Calculer le nombre maximal de certifications reçues par l'identité courante
    if ( identity.nbCert > nbMaxCertifs) {
      nbMaxCertifs = identity.nbCert;
    }

    // calculate countMembersWithSigQtyValidCert
    if ( identity.nbValidPendingCert >= conf.sigQty) {
      countMembersWithSigQtyValidCert++;
    }
  }  // END IDENTITIES LOOP

  // Si demandé, retrier les, certifications par date de disponibilité
  if (sortSig == "Availability") {
    idtysPendingCertifsList = triParDateDeDisponibilite(idtysPendingCertifsList, conf, currentBlockchainTimestamp, identitiesList)
  }

  // Récupérer la valeur du critère de tri pour chaque identité
  let tabSort = getSorting(identitiesList, sort_by, currentBlockchainTimestamp, conf)

  // Trier les identités
  idtysListOrdered = await getIdentityListOrdered(
      identitiesList,
      idtysPendingCertifsList,
      currentBlockchainTimestamp,
      currentMembersCount,
      limitTimestamp,
      wotbInstance,
      duniterServer,
      conf,
      dSen,
      tabSort,
      order)

  console.log('Calcul des qualités...')

  membersQualityExt = getMembersQualityExt(wotbInstance, idtysListOrdered, conf, dSen)

  console.log('Calcul des moyennes...')
  const meansAndCounts = computeMeansAndCounts(idtysListOrdered, nbMaxCertifs)
  meanSentriesReachedByIdtyPerCert = meansAndCounts.meanSentriesReachedByIdtyPerCert
  meanMembersReachedByIdtyPerCert = meansAndCounts.meanMembersReachedByIdtyPerCert

  showExecutionTimes()
}
