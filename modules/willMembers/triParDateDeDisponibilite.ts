import {PendingCert, WillMemberIdentity} from "./interfaces";
import {ConfDTO} from "duniter/app/lib/dto/ConfDTO";

export function triParDateDeDisponibilite(idtysPendingCertifsList: PendingCert[][],
                                          conf: ConfDTO,
                                          currentBlockchainTimestamp: number,
                                          identitiesList: WillMemberIdentity[]) {

  const idtysPendingCertifsListSort: PendingCert[][] = [ [] ];
  for (var i=0;i<idtysPendingCertifsList.length;i++)
  {
    idtysPendingCertifsListSort[i] = Array();
    let min;
    let idMin =0;
    let tmpExcluded = Array();
    for (let j=0;j<idtysPendingCertifsList[i].length;j++) { tmpExcluded[j] = false; }
    for (let j=0;j<idtysPendingCertifsList[i].length;j++)
    {
      min = currentBlockchainTimestamp+conf.sigValidity; // begin to min = max

      // search idMin (id of certif with min timestampWritable)
      for (let k=0;k<idtysPendingCertifsList[i].length;k++)
      {
        if (idtysPendingCertifsList[i][k].timestampWritable < min && !tmpExcluded[k])
        {
          min = idtysPendingCertifsList[i][k].timestampWritable;
          idMin = k;
        }
      }

      // Push min value on sort table
      idtysPendingCertifsListSort[i].push({
        from: idtysPendingCertifsList[i][idMin].from,
        wotb_id: idtysPendingCertifsList[i][idMin].wotb_id,
        issuerIsSentry: idtysPendingCertifsList[i][idMin].issuerIsSentry,
        blockNumber: idtysPendingCertifsList[i][idMin].blockNumber,
        creationTimestamp: idtysPendingCertifsList[i][idMin].creationTimestamp,
        timestampExpire: idtysPendingCertifsList[i][idMin].timestampExpire,
        timestampWritable: idtysPendingCertifsList[i][idMin].timestampWritable,
        validBlockStamp: idtysPendingCertifsList[i][idMin].validBlockStamp
      });

      // Calculer la date de disponibilité du dossier d'inscription de l'identité correspondante
      // := date de disponibilité maximale parmi les sigQty certifications aux dates de disponibilités les plus faibles
      if (j<conf.sigQty)
      {
        let timestampWritable = idtysPendingCertifsList[i][idMin].timestampWritable;
        identitiesList[i].registrationAvailability = (timestampWritable > identitiesList[i].registrationAvailability) ? timestampWritable : identitiesList[i].registrationAvailability;
      }

      // Exclure la valeur min avant de poursuivre le tri
      tmpExcluded[idMin] = true;
    }

  }
  return idtysPendingCertifsListSort;
}