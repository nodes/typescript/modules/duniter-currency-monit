export class SentryChecker {

  private sentriesIndex: { [k: string]: boolean } = {}


  constructor(private sentries: number[]) {
  }

  isIssuerSentry(issuerPubkey: string, wotb_id: number): boolean {
    if (typeof(this.sentriesIndex[issuerPubkey]) == 'undefined') {
      this.sentriesIndex[issuerPubkey] = false;
      for (let s=0;s<this.sentries.length;s++) {
        if (this.sentries[s] == wotb_id) {
          this.sentriesIndex[issuerPubkey] = true;
          this.sentries.splice(s, 1);
          return true
        }
      }
      return false
    }
    else {
      return this.sentriesIndex[issuerPubkey];
    }
  }
}