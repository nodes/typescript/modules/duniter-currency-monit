import {Wot, WotBuilder} from "duniter/neon/lib";
import {DetailedDistance, WillMemberIdentityWithPendingCerts} from "./interfaces";
import {ConfDTO} from "duniter/app/lib/dto/ConfDTO";

export function getMembersQualityExt(wotbInstance: Wot,
                                     idtysListOrdered: WillMemberIdentityWithPendingCerts[],
                                     conf: ConfDTO,
                                     dSen: number
): { [k: string]: string } {
  let membersQualityExt: { [k: string]: string } = {}
  const nbIdentites = idtysListOrdered.length
  let i = 0
  idtysListOrdered.forEach(pendingIdty => {
    console.log(`Qualité de l'identité ${i+1}/${nbIdentites}`)
    // Créer une wot temporaire
    let tmpWot = WotBuilder.fromWot(wotbInstance);
    // Mesurer la qualité externe de chaque emetteur de chaque certification
    for (const cert of pendingIdty.pendingCertifications) {
      if (typeof (membersQualityExt[cert.from]) == 'undefined') {
        const detailedDistanceQualityExt: DetailedDistance = tmpWot.detailedDistance(cert.wotb_id, dSen, conf.stepMax - 1, conf.xpercent);
        membersQualityExt[cert.from] = ((detailedDistanceQualityExt.nbSuccess / detailedDistanceQualityExt.nbSentries) / conf.xpercent).toFixed(2);
      }
    }
    // Vider la mémoire
    // tmpWot.clear()
    i++
  })
  return membersQualityExt
}