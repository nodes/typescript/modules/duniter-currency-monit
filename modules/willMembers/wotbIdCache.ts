import {DataFinder} from "../../lib/DataFinder";

export class WotbIdCache {

  private wotbIdIndex: { [k: string]: number | undefined } = {}
  private dataFinder: DataFinder


  constructor(dataFinder: DataFinder) {
    this.dataFinder = dataFinder;
  }

  async getWotbId(pubkey: string): Promise<number> {
    if (this.wotbIdIndex[pubkey] === undefined) {
      this.wotbIdIndex[pubkey] = await this.dataFinder.getWotbIdByIssuerPubkey(pubkey);
    }
    return this.wotbIdIndex[pubkey] as number
  }
}