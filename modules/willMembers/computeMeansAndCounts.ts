import {WillMemberIdentityWithPendingCerts} from "./interfaces";

export function computeMeansAndCounts(idtysListOrdered: WillMemberIdentityWithPendingCerts[], nbMaxCertifs: number) {
  const meanSentriesReachedByIdtyPerCert: number[] = []
  const meanMembersReachedByIdtyPerCert: number[] = []
  const countIdtiesPerReceiveCert: number[] = []
  // Réinitialiser sumSentriesReachedByIdtyPerCert, sumMembersReachedByIdtyPerCert et countIdtiesPerReceiveCert
  for (let i=0; i<=nbMaxCertifs; i++) {
    meanSentriesReachedByIdtyPerCert[i] = 0;
    meanMembersReachedByIdtyPerCert[i] = 0;
    countIdtiesPerReceiveCert[i] = 0;
  }

  idtysListOrdered.forEach(pendingIdty => {
    // Si le cache a été réinitialiser, recalculer les sommes meanSentriesReachedByIdtyPerCert et meanMembersReachedByIdtyPerCert
    if (pendingIdty.nbValidPendingCert > 0) {
      let nbReceiveCert = pendingIdty.nbValidPendingCert;
      meanSentriesReachedByIdtyPerCert[nbReceiveCert-1] += pendingIdty.percentSentriesReached;
      meanMembersReachedByIdtyPerCert[nbReceiveCert-1] += pendingIdty.percentMembersReached;
      countIdtiesPerReceiveCert[nbReceiveCert-1] += 1;
    }
  })

  // Calculate meanSentriesReachedByIdtyPerCert and meanMembersReachedByIdtyPerCert
  for (let i = 0; i <= nbMaxCertifs; i++) {
    if (countIdtiesPerReceiveCert[i] > 0) {
      meanSentriesReachedByIdtyPerCert[i] = parseFloat((meanSentriesReachedByIdtyPerCert[i] / countIdtiesPerReceiveCert[i]).toFixed(2));
      meanMembersReachedByIdtyPerCert[i] = parseFloat((meanMembersReachedByIdtyPerCert[i] / countIdtiesPerReceiveCert[i]).toFixed(2));
    }
    else {
      meanSentriesReachedByIdtyPerCert[i] = 0.0;
      meanMembersReachedByIdtyPerCert[i] = 0.0;
    }
  }

  return {
    meanSentriesReachedByIdtyPerCert,
    meanMembersReachedByIdtyPerCert
  }
}