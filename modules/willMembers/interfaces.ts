import {DBMembership} from "duniter/app/lib/dal/sqliteDAL/MembershipDAL";

export interface PendingCert {
  from: string
  pubkey?: string
  wotb_id: number
  issuerIsSentry: boolean
  blockNumber: number
  creationTimestamp: number
  timestampExpire: number
  timestampWritable: number
  validBlockStamp: boolean
}

export interface DetailedDistance {
  nbSentries: number;
  nbSuccess: number;
  nbSuccessAtBorder: number;
  nbReached: number;
  nbReachedAtBorder: number;
  isOutdistanced: number;
}

export interface WillMemberIdentity {
  BlockNumber: number
  creationTimestamp: number
  pubkey: string
  uid: string
  hash?: string
  wotexId: string
  expires_on: number
  nbCert: number
  nbValidPendingCert: number
  registrationAvailability: number
  detailedDistance?: DetailedDistance
  pendingCertifications?: PendingCert[]
  validBlockStamp: boolean
  idtyRevoked: boolean
  percentSentriesReached?: number
  percentMembersReached?: number
  membership?: DBMembership|null
}

export interface WillMemberIdentityWithPendingCerts extends WillMemberIdentity {
  pendingCertifications: PendingCert[]
  percentSentriesReached: number
  percentMembersReached: number
}