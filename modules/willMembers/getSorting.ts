import {WillMemberIdentity} from "./interfaces";
import {ConfDTO} from "duniter/app/lib/dto/ConfDTO";

export function getSorting(identitiesList: WillMemberIdentity[], sort_by: string, currentBlockchainTimestamp: any, conf: ConfDTO): number[] {
  let tabSort: number[] = []
  if (sort_by == "creationIdty") {
    tabSort = identitiesList.map(i => i.expires_on)
  }
  else if (sort_by == "sigCount" || sort_by == "registrationPackage") {
    for (const idty of identitiesList) {
      // Calculate registrationAvailabilityDelay
      let registrationAvailabilityDelay = (idty.registrationAvailability > currentBlockchainTimestamp) ? (idty.registrationAvailability-currentBlockchainTimestamp):0;

      // Trier les identités par date de disponibilité de leur dossier d'inscription (le signe moins est nécessaire car plus un dossier est disponible tôt
      //  plus la valeur de registrationAvailabilityDelay sera petite, hors le nombre obtenu est classé de façon décroissante)
      // Attribuer un malus de 2*sigValidity secondes par certification valide (plafonner à sigQty dans le cas de 'registrationPackage')
      if (sort_by == "registrationPackage" && idty.nbValidPendingCert > conf.sigQty) {
        tabSort.push(-registrationAvailabilityDelay + (2*conf.sigValidity*conf.sigQty));
      }
      else {
        tabSort.push(-registrationAvailabilityDelay + (2*conf.sigValidity*idty.nbValidPendingCert));
      }
    }
  }
  return tabSort
}