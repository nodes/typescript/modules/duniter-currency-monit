import {DBMembership} from "duniter/app/lib/dal/sqliteDAL/MembershipDAL";
import {Wot, WotBuilder} from "duniter/neon/lib";
import {PendingCert, WillMemberIdentity, WillMemberIdentityWithPendingCerts} from "./interfaces";
import {Server} from "duniter/server";
import {ConfDTO} from "duniter/app/lib/dto/ConfDTO";

export async function getIdentityListOrdered(identitiesList: WillMemberIdentity[],
                                       idtysPendingCertifsList: PendingCert[][],
                                       currentBlockchainTimestamp: number,
                                       currentMembersCount: number,
                                       limitTimestamp: number,
                                       wotbInstance: Wot,
                                       duniterServer: Server,
                                       conf: ConfDTO,
                                       dSen: number,
                                       tabSort: number[],
                                       order: string
): Promise<WillMemberIdentityWithPendingCerts[]> {

  let idtysListOrdered: WillMemberIdentityWithPendingCerts[] = []
  for (let i=0; i<identitiesList.length; i++) {
    let max = -1;
    let idMax =0;
    for (let j=0; j<identitiesList.length; j++) {
      if (tabSort[j] > max) {
        max = tabSort[j];
        idMax = j;
      }
    }
    const pendingIdty = identitiesList[idMax]
    const certsToPending = idtysPendingCertifsList[idMax]

    // Push max value on sort table, only if respect days limit
    if (limitTimestamp <= pendingIdty.expires_on) {
      // Exclure la valeur max avant de poursuivre le tri
      tabSort[idMax] = -1;
      console.log(`Traitement de l'identité ${i+1}/${identitiesList.length} (${pendingIdty.uid}) : expirée`)
      continue;
    }

    // Vérifier que cette identité n'a pas déjà été prise en compte (empecher les doublons)
    let doubloon = idtysListOrdered.filter(idty => pendingIdty.uid == idty.uid && pendingIdty.BlockNumber == idty.BlockNumber).length;
    if (doubloon) {
      // Exclure la valeur max avant de poursuivre le tri
      tabSort[idMax] = -1;
      console.log(`Traitement de l'identité ${i+1}/${identitiesList.length} (${pendingIdty.uid}) : doublon`)
      continue;
    }
    console.log(`Traitement de l'identité ${i+1}/${identitiesList.length} (${pendingIdty.uid})...`)

    // Push max value on sort table (and test distance rule)

    // Tester la présence de l'adhésion
    const pendingMembershipsOfIdty: DBMembership[] = await duniterServer.dal.msDAL.getPendingINOfTarget(pendingIdty.hash as string);
    let membership: DBMembership|null = pendingMembershipsOfIdty.filter(ms => ms.expires_on > currentBlockchainTimestamp)[0]

    // Créer une wot temporaire
    let tmpWot = WotBuilder.fromWot(wotbInstance);

    // Ajouter un noeud a la wot temporaire et lui donner toute les certifications valides reçues par l'indentité en attente
    let pendingIdtyWID = tmpWot.addNode();
    for (const cert of certsToPending) {
      if (cert.validBlockStamp) {
        tmpWot.addLink(cert.wotb_id, pendingIdtyWID);
      }
    }
    // Récupérer les données de distance du dossier d'adhésion de l'indentité idMax
    let detailedDistance = tmpWot.detailedDistance(pendingIdtyWID, dSen, conf.stepMax, conf.xpercent);

    // Nettoyer la wot temporaire
    tmpWot.clear();

    // Calculer percentSentriesReached et percentMembersReached
    let percentSentriesReached = parseFloat(((detailedDistance.nbSuccess/detailedDistance.nbSentries)*100).toFixed(2));
    let percentMembersReached = parseFloat(((detailedDistance.nbReached/currentMembersCount)*100).toFixed(2));

    // Pousser l'identité dans le tableau idtysListOrdered
    idtysListOrdered.push({
      uid: pendingIdty.uid,
      wotexId: pendingIdty.wotexId,
      creationTimestamp: pendingIdty.creationTimestamp,
      pubkey: pendingIdty.pubkey,
      BlockNumber: pendingIdty.BlockNumber,
      expires_on: pendingIdty.expires_on,
      nbCert: pendingIdty.nbCert,
      registrationAvailability: pendingIdty.registrationAvailability,
      nbValidPendingCert: pendingIdty.nbValidPendingCert,
      detailedDistance,
      percentSentriesReached,
      percentMembersReached,
      membership,
      pendingCertifications: certsToPending,
      validBlockStamp: pendingIdty.validBlockStamp,
      idtyRevoked: pendingIdty.idtyRevoked
    });
    // Exclure la valeur max avant de poursuivre le tri
    tabSort[idMax] = -1;
  }

  // Si ordre croissant demandé, inverser le tableau
  if (order == 'asc') {
    const idtysListOrdered2: WillMemberIdentityWithPendingCerts[] = []
    let tmpIdtysListOrderedLength = idtysListOrdered.length;
    for (let i=0;i<tmpIdtysListOrderedLength;i++) {
      idtysListOrdered2[i] = idtysListOrdered[tmpIdtysListOrderedLength-i-1];
    }
    idtysListOrdered = idtysListOrdered2;
  }

  return idtysListOrdered
}